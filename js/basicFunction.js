
var cssRedBorder = 'redBorder';

function errorAlert(title, text, delay) {
    swal({
        title: title,
        text: text,
        timer: delay,
        type: "error",
        showConfirmButton: false
    });
}

function successAlert(title, text, delay) {
    swal({
        title: title,
        text: text,
        timer: delay,
        type: 'success',
        showConfirmButton: false
    });
}

function successAlertOnlyHeader(textH1, delay) {
    swal({
        title: textH1,
        timer: delay,
        type: 'success',
        showConfirmButton: false
    });
}

function errorAlertOnlyHeader(textH1, delay) {
    swal({
        title: textH1,
        timer: delay,
        type: 'error',
        showConfirmButton: false
    });
}

function isNotEmpty(value) { // is Empty, is Blank(?)
    return "" != value && null != value;
}

function objectIsNotEmpty(value){
    return null != value;
}

function addCssClass(id, cssClass){
    if( !isSetCssClass(id, cssClass) )
        $(id).addClass(cssClass);
}

function removeCssClass(id, cssClass){
    if( isSetCssClass(id, cssClass) )
        $(id).removeClass(cssClass);
}

function isSetCssClass(id, cssClass){
    return $(id).hasClass(cssClass);
}

function removeCssClassFromManyId(array, cssClass){
    for(var i=0 ; i<array.length ; i++)
        removeCssClass(array[i], cssClass);
}

function addCssClassToManyId(array, cssClass){
    for(var i=0 ; i<array.length ; i++)
        addCssClass(array[i], cssClass);
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function checkField(id, text, cssClass) {
    if(isNotEmpty(text)){
        removeCssClass(id, cssClass);
        return true;
    }
    addCssClass(id, cssClass);
    return false;
}

function equals(val1, val2) {
    return val1 == val2;
}

function setVisibleIsNotEmpty(id , array){
    if(Object.keys(array).length == 0)
        $(id).addClass('hid');
    else
        $(id).removeClass('hid');
}