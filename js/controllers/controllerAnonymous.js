'use strict';

app.controller('anonymousPanel', ['$scope', '$http', 'localStorageService', function ( $scope, $http, localStorageService ) {
    
	function auth( login, password ) {
        $http({
            method: 'POST',
            url: serverIp+'/login',
            data: { 
				login : login, 
				password : password 
			}
        }).then(function successCallback(response) {
            localStorageService.set('auth-user', { 
                login: login, 
                token: response.data.text 
            });
            window.location.href='userPanel.html';
        }, function errorCallback(response) {
			if(isNotEmpty(response.data))
                errorAlertOnlyHeader(response.data.text, delay);
        });
    }

    function registrationUser( login, password, passwordAgain ) {
        $http({
            method: 'POST',
            url: serverIp+'/registration',
            data: { 
				login : login, 
				password : password, 
				passwordAgain : passwordAgain 
			}
        }).then(function successCallback(response) {
            successAlertOnlyHeader(response.data.text, delay);
        }, function errorCallback(response) {
			if(isNotEmpty(response.data))
				errorAlertOnlyHeader(response.data.text, delay);
        });
    }

    $scope.login = function() {
        if( checkField('#authLogin', $scope.authLogin, cssRedBorder) && checkField('#authPassword', $scope.authPassword, cssRedBorder) )
            auth($scope.authLogin, $scope.authPassword);    
    }

    $scope.registrationUser = function() {
        if( checkField('#registrationLogin', $scope.registrationLogin, cssRedBorder) && checkField('#registrationPass', $scope.registrationPass, cssRedBorder) && checkField('#registrationPass2', $scope.registrationPass2, cssRedBorder) )
            if( equals($scope.registrationPass, $scope.registrationPass2) ) {
                removeCssClassFromManyId(['#registrationPass', '#registrationPass2'], cssRedBorder);
                registrationUser($scope.registrationLogin, $scope.registrationPass, $scope.registrationPass2);
            }else
                addCssClassToManyId(['#registrationPass', '#registrationPass2'], cssRedBorder);
    }

}]);
