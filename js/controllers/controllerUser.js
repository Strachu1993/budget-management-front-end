'use strict';

app.controller('userPanel', ['$scope', '$http', '$filter', 'localStorageService', function ( $scope, $http, $filter, localStorageService ) {
   
    var month = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];
    
    var changeText = 'Dokonano zmiany';
    var addedText = 'Dodano';

    var userPath = '/user';
    $scope.edit = false;
    $scope.memories;
    $scope.memoriesTwo;
    $scope.memoriesThried;
    $scope.addBudgetRows;
    
    $('#addBudgetPanel').hide();
    $('#editBudget').hide();
    $('#userName').append(localStorageService.get('auth-user').login);
    
    $scope.typeOfExpense = [
        {value: 'MINUS'},
        {value: 'PLUS'}
    ];

    // SCROOL TO begin
    jQuery(function($){
        
            $.scrollTo(0); // reset scrolla 
            
            $('.scrollUp').click(function() { 
                $.scrollTo($('body'), 750);
            });
            
        }
    );

    // pokaz podczas przewijania
    $(window).scroll(function(){
            if($(this).scrollTop() > window.innerWidth / 2)
                $('.scrollUp').fadeIn();
            else
                $('.scrollUp').fadeOut();
        }
    );
// SCROOL TO end

// CHART begin

    $scope.getDateByBudget = function(budgetDate){
        return month[budgetDate.monthValue-1] + '-' + budgetDate.year;
    }

    $scope.first = false;

    $scope.chartLines = function(destiny, budgets){
    
        var date = [];
        var sum = [];
        var plus = [];
        var minus = [];

        angular.forEach(budgets, function(value, index){
            date.push($scope.getDateByBudget(value.date));
            sum.push(value.sum);
            plus.push(value.plusSum);
            minus.push(value.minusSum);
            //alert($scope.getDateByBudget(value.date));
        })

        var config = {
            type: 'line',
            data: {
                labels: date,
                datasets: [{
                    label: "Suma",
                    backgroundColor: window.chartColors.yellow,
                    borderColor: window.chartColors.yellow,
                    fill: false,
                    data: sum
                },
                {
                    label: "Plus",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    fill: false,
                    data: plus
                },
                {
                    label: "Minus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    fill: false,
                    data: minus
                }
                ]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Wykres miesięcznych wydatków'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Miesiąc'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Suma wydatków'
                        }
                    }]
                }
            }
        };

        // document.getElementById(destiny).innerHTML = "";
        // document.getElementById(destiny).getContext("2d").innerHTML = "";
        var ctx = document.getElementById(destiny).getContext("2d");
        window.myLine = new Chart(ctx, config);
    }

    $scope.rowChartPie = function(destiny, budget){

        var des = destiny+''+$scope.getDateByBudget(budget.date);
            alert(des);

            var config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            33,
                            44,
                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.green
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "Plusy",
                        "Minusy",
                    ]
                },
                options: {
                    responsive: true
                }
            };
    
            var ctx = document.getElementById(des).getContext("2d");
            window.myPie = new Chart(ctx, config);
    }

// CHART end

    $scope.getFixedExpenses = function() {
        $http({
             method: 'GET',
             url: serverIp + userPath + '/getFixedExpenses'
        }).then(function successCallback(success) {
            $scope.fixedExpenses = success.data;
            //setVisibleIsNotEmpty("#table-fixedExpense",  $scope.fixedExpenses);
        }, function errorCallback(error) {});
     }

     $scope.getBudgets = function() {
        $http({
             method: 'GET',
             url: serverIp + userPath + '/getBudget'
        }).then(function successCallback(success) {
            $scope.budgets = success.data;
            //setVisibleIsNotEmpty("#table-budget",  $scope.budgets);
            $scope.chartLines("mainCanvas", $scope.budgets);
            // $scope.rowChartPie('rowCanvas', $scope.budgets);
        }, function errorCallback(error) {});
     }

    $scope.getBudgets();
    $scope.getFixedExpenses();

    $(function () {
        $('#dateTimePickerBudget').datetimepicker({
            viewMode: 'years',
            format: 'MM-YYYY',
            locale: 'pl'
        });
    });

	$scope.logout = function(){
		localStorageService.remove('auth-user');
        window.location.href='index.html';
	}

	function changePassword(oldPass, newPass, repeatNewPass) {
       $http({
            method: 'PUT',
            url: serverIp + userPath + '/changePassword',
            data: { 
                oldPassword : oldPass, 
                newPassword : newPass,
                repeatPassword: repeatNewPass
            }
       }).then(function successCallback(success) {
           successAlertOnlyHeader(success.data.text, delay)
       }, function errorCallback(error) {
           if(isNotEmpty(error.data))
               errorAlertOnlyHeader(error.data.text, delay);
       });
    }

    $scope.changeUserPass = function() {
       if(checkField('#oldPass', $scope.oldPass, cssRedBorder) && checkField('#newPass', $scope.newPass, cssRedBorder) && checkField('#newPass2', $scope.newPass2, cssRedBorder)){
           if(equals($scope.newPass, $scope.newPass2)) {
               removeCssClassFromManyId(['#newPass', '#newPass2'], cssRedBorder);
               changePassword( $scope.oldPass, $scope.newPass, $scope.newPass2);
           }else
               addCssClassToManyId(['#newPass', '#newPass2'], cssRedBorder);
        }
    }

    function changeFixedExpense(id, name, value, type){
        $http({
            method: 'PUT',
            url: serverIp + userPath + '/changeFixedExpenses',
            data: { 
                id: id,
                name : name, 
                value : value,
                type: type
            }
       }).then(function successCallback(success) {
           $scope.fixedExpenses.splice(id-1 ,1 , success.data);
           successAlertOnlyHeader(changeText, delay);
       }, function errorCallback(error) {});
    }    
    
    $scope.changeFixedExpense = function(fixedExpense, bool ,memories , memoriesTwo, memoriesThried) {
        if(!bool && isNotEmpty(memories) && (fixedExpense.name != memories || fixedExpense.value != memoriesTwo || fixedExpense.type != memoriesThried)) {
            changeFixedExpense(fixedExpense.id, memories, memoriesTwo, memoriesThried);
        }
    } 
//////////////////////// DELETE begin
    function deleteFixedExpenses(idScope, fixedExpense){
        $http({
                method: 'DELETE',
                url: serverIp + userPath + '/deleteFixedExpenses',
                params: {
                    id: fixedExpense.id
                }
            }).then(function successCallback(response) {
                $scope.fixedExpenses.splice(idScope, 1);
                setVisibleIsNotEmpty("#table-fixedExpense",  $scope.fixedExpenses);
                successAlert(response.data.text , fixedExpense.name, delay);
            }, function errorCallback(response) {
                errorAlert("Błąd ", response.data, delay);
            });
    }

    $scope.deleteFixedExpenses = function(idScope, fixedExpense) {    
        swal({
            title: "Usuń",
            text: "Nazwa: " + fixedExpense.name,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Tak",
            cancelButtonText: "Nie",
            closeOnConfirm: false,
        },
        function(){
            deleteFixedExpenses(idScope, fixedExpense);
        });
    } 
//////////////////////// DELETE end
/////////////////////// ADD begin

    $scope.addFixedExpenses = function(){
        if(checkField('#newFixedExpensesName', $scope.newFixedExpensesName, cssRedBorder))
            if(checkField('#newFixedExpensesValue', $scope.newFixedExpensesValue, cssRedBorder)){
                    $http({
                        method: 'POST',
                        url: serverIp + userPath + '/addFixedExpenses',
                        data: { 
                            name : $scope.newFixedExpensesName, 
                            value : $scope.newFixedExpensesValue,
                            type: $scope.newFixedExpensesType
                        }
                    }).then(function successCallback(success) {
                            $scope.newFixedExpensesName = '';
                            $scope.newFixedExpensesValue = '';
                            $scope.fixedExpenses.splice($scope.fixedExpenses.length, 0, success.data);
                            setVisibleIsNotEmpty("#table-fixedExpense",  $scope.fixedExpenses);
                            successAlertOnlyHeader(addedText, delay);
                    }, function errorCallback(error) {
                        if(isNotEmpty(error.data))
                            errorAlertOnlyHeader(error.data.text, delay);
                    });
                }
    }

//////////////////////// ADD end
/////////////////////////// BUDGET begin

    $scope.addBudgetButton = function(){

        var addBudgetDom = $('#addBudgetPanel');

        if(addBudgetDom.is(":visible")){
            addBudgetDom.hide();
        }else{
            addBudgetDom.show();
            $scope.addBudgetRows = angular.copy($scope.fixedExpenses);
        }
        
    }

    $scope.deleteBudgetRow = function(index){
        $scope.addBudgetRows.splice(index, 1);
        //setVisibleIsNotEmpty("#table-addBudget",  $scope.addBudgetRows);
    }

    $scope.addBudgetRow = function(){
        $scope.addBudgetRows.splice($scope.fixedExpenses.length, 0, {name:"", value:"", type:$scope.typeOfExpense[0].value});
    }

    $scope.sendBudgetRow = function(){
        $http({
            method: 'POST',
            url: serverIp + userPath + '/addBudget',
            data: $scope.addBudgetRows,
            params: {
                "date": $('#dateTimePickerBudget').find("input").val()
            }
        }).then(function successCallback(success) {
                $scope.getBudgets();
                $('#addBudget').hide();
                successAlertOnlyHeader(addedText, delay);
        }, function errorCallback(error) {
            if(isNotEmpty(error.data))
                errorAlertOnlyHeader(error.data.text, delay);
        });

    }

    $scope.editBudgetButton = function(date){
        var editBudgetDom = $('#editBudget');
        
        if(editBudgetDom.is(":visible")){
            editBudgetDom.hide();
        }else{
            editBudgetDom.show();
            $.scrollTo($('#editBudget'), 750); 
            $http({
                method: 'GET',
                url: serverIp + userPath + '/getBudgetByDate',
                params: {
                    date: date.monthValue +"-"+ date.year
                }
            }).then(function successCallback(response) {
                $scope.editBudgetByDate = response.data;
            }, function errorCallback(response) {
                errorAlert("Błąd ", response.data, delay);
            });
        }
    }

    $scope.deleteBudgetById = function(budget, index){   
        swal({
            title: "Usuń",
            text: "Nazwa: " + budget.name,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Tak",
            cancelButtonText: "Nie",
            closeOnConfirm: false,
        },
        function(){
            $http({
                method: 'DELETE',
                url: serverIp + userPath + '/deleteBudgetById',
                params: {
                    id: budget.id
                }
            }).then(function successCallback(response) {
                $scope.editBudgetByDate.splice(index, 1);
                $scope.getBudgets();
                successAlertOnlyHeader("Usunięto", delay);
            }, function errorCallback(response) {
                errorAlert("Błąd ", response.data, delay);
            });
        });
    }

    $scope.changeBudgetById = function(budget, bool, index, memories, memoriesTwo, memoriesThried){
        if(!bool && isNotEmpty(memories) && isNotEmpty(memoriesTwo) && (budget.name != memories || budget.value != memoriesTwo || budget.type != memoriesThried)){
            $http({
                method: 'PUT',
                url: serverIp + userPath + '/changeBudget',
                data: {
                    id: budget.id,
                    name: memories,
                    value: memoriesTwo,
                    type: memoriesThried
                }
            }).then(function successCallback(response) {
                $scope.editBudgetByDate.splice(index, 1, response.data);
                $scope.getBudgets();
                successAlertOnlyHeader(changeText, delay);
            }, function errorCallback(response) {});
        }
    }

    /////////////////// BUDGET end

}]);