'use strict';

var delay = 2000;
var serverIp = 'http://localhost:8080';

var app = angular.module('app', [
        'LocalStorageModule'
	]);

app.factory('authInterceptorService', ['$q', 'localStorageService', function ( $q, localStorageService ) {

    return {
        request: function (config) { //zapytanie
            config.headers = config.headers || {};
            var userLogin = localStorageService.get('auth-user');
			
            if(objectIsNotEmpty(userLogin))
				config.headers['auth-token'] = userLogin.token;
				
            return config;
        },

        response: function (result) { //odpowiedz
            return result;
        },

        responseError: function (rejection) { //error
            if(rejection.status === -1) {
                //console.log(errorConnection, rejection.status, 'status');
                errorAlertOnlyHeader('Błąd połączenia', delay); // errorConnection
            }

            if(rejection.status === 400){
                errorAlertOnlyHeader('Złe dane', delay);
            }
            //if (rejection.status === 401) {
                //console.log(errorUnauthorized, rejection.status, 'status');
            //    errorAlertOnlyHeader('Błąd logowania ddd', delay); //errorUnauthorized
            //}

            return $q.reject(rejection); // .data.stringWrapper
        }
    }
	
}]);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
}]);   
//     $httpProvider
//     .when('/', {
//         templateUrl: 'index.html'
//     })
//     .when('/user',{
//         templateUrl: 'userPanel'
//     })
//     .otherwise({
//         redirectTo: '/'
//     });

// }]);